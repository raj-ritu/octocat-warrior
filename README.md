# octocat-warrior

**How to compile** 

1. cd build
1. run the command "cmake .. -DCMAKE_BUILD_TYPE=Debug -G "Unix Makefiles" to compile
1. run the command "make all" to make the build
1. Execute "./octocat-warrior_run" in the build dirctory to run the code

# How to use
This program will sum the user input numbers and it gives the option to user to make choice of sum operation

1. input the number of integers
1. choice of summation


# PROJECT STRUCTURE 

```
octocat-warrior/
├── build
├── CMakeLists.txt
├── lib
│   └── googletest
├── README.md
├── src
│   ├── CMakeLists.txt
│   └── kLensTask.cpp
└── tst
    ├── CMakeLists.txt
    └── main.cpp
```


