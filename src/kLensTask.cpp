#include <iostream>
#include <numeric>


using namespace std;


int main()
{
    cout << "\n\nWelcome to KLens-Task :-)\n\n\n";
    cout << " =====  Program to find the Sum of n numbers entered by the user  ===== \n\n";
    
    std::cout << "CHOOSE A OPTION FOR SUMMATION:\n";
    std::cout << "[1] Pointer\n"
	      << "[2] Library\n";

    
    //variable declaration
    int n,i,temp,arr[50];

    //As we are dealing with the sum, so initializing with 0.
    int sum = 0;


    //Choice for summation , here user can choose an option to add number
    std::string input;
    std::getline(std::cin, input);

    
    while(input != "1" && input != "2") {
	    std::cout << "Invalid!\n\n";


   	    std::cout << "CHOOSE A OPTION FOR SUMMATION:\n";
            std::cout << "[1] Pointer\n"
                      << "[2] Library\n";
	    std::getline(std::cin, input);
    }

    //variable declaration
    //int n,i,temp;

    //As we are dealing with the sum, so initializing with 0.
    //int sum = 0;
    
     
    if (input == "1") {

    	//taking input from the command line (user)
    	cout << " Enter the number of integers you want to add : ";
    	cin >> n;
    	cout << "\n\n";

    	//taking n numbers as input from the user and adding them to find the final sum
    	for(i=0;i<n;i++)
    	{
        	cout << "Enter number" << i+1 << " :  ";
        	cin >> temp;
        	//add each number to the sum of all the previous numbers to find the final sum
        	sum += temp;
   	}
    
    	cout << "\n\nSum of the " << n << " numbers entered by the user is : "<< sum << endl;
    	cout << "\n\n\n";
    } else if (input == "2") {
	       cout << " Enter the number of integers you want to add : ";
               cin >> n;
               cout << "\n\n";

	       
       	      for(i=0;i<n;i++)
             {      
		     cout << "Enter number" << i+1 << " :  ";
		     cin >> arr[i];
                     //add each number to the sum of all the previous numbers to find the final sum
                     sum = sum + arr[i];
             }
	      cout <<"The libraries sum is: " << sum;
              cout << "\n\n\n";

    }


    return 0;
}
